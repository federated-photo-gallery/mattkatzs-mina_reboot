from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^mina_reboot/', include('mina_reboot.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/(.*)', admin.site.root),

    # OpenID views
    (r'^openid/', include('django_openid_auth.urls')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_DOC_ROOT}),
    (r'^dash/$', 'mina_reboot.photos.views.dashboard'),
    (r'^dash/profile/$', 'mina_reboot.photos.views.manage_profile'),
    (r'^dash/upload/$', 'mina_reboot.photos.views.upload_photo'),
    (r'^(?P<username>[\_\d\w]+)/$', 'mina_reboot.photos.views.profile'),
    (r'^(?P<username>[\_\d\w]+)/photos/$', 'mina_reboot.photos.views.photostream'),
    (r'^(?P<username>[\_\d\w]+)/photos/(?P<slug>[\-\d\w]+)/$', 'mina_reboot.photos.views.photo_by_user_and_slug'),
    (r'^[\_\d\w]+/photos/(?P<slug>[\-\d\w]+)/edit/$', 'mina_reboot.photos.views.manage_photo'),

    (r'^$', 'mina_reboot.photos.views.dashboard'),
)
