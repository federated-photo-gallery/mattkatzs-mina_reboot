from django.db import models
from django.forms import ModelForm
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ImageModel
from utils import EXIF

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True, editable=False)

    homepage = models.URLField(blank=True)
    country = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100, blank=True)

class UserProfileForm(ModelForm):
    class Meta:
        model = UserProfile

class Photo(ImageModel):
    name = models.CharField(max_length=100)
    description = models.TextField()
    original_image = models.ImageField(upload_to='photos')
    num_views = models.PositiveIntegerField(editable=False, default=0)
    user = models.ForeignKey(User)
    slug_base = models.SlugField(_('slug base'), editable=False, null=True,
                                 help_text=_('This is the slug form of the title, to serve as the root for the image slug.'))
    slug_iter = models.PositiveIntegerField(editable=False, null=True, default=0)
    slug = models.SlugField(_('slug'), editable=False, null=True)

    def __unicode__(self):
        return self.name

    @property
    def EXIF(self):
        try:
            return EXIF.process_file(open(self.original_image.path, 'rb'))
        except:
            try:
                return EXIF.process_file(open(self.original_image.path, 'rb'), details=False)
            except:
                return {}

    '''
    @property
    def slug(self):
        if self.slug_iter is 0:
            return self.slug_base
        else:
            return '%s-%d' % (self.slug_base, self.slug_iter)
    '''

    def save(self, *args, **kwargs):
        # Create the slug, using a slugified photo name and a counter
        if self.slug_base is None:
            # Set the slug base to be the slugification of the photo name
            self.slug_base = slugify(self.name)
            
            # If there are multiple photos by this user with the same name,
            # tack a counter on the end to give them unique slugs.
            try: 
                q = Photo.objects.filter(user__username=self.user.username).filter(slug_base=self.slug_base).order_by('slug_iter').reverse()[:1]
            except:
                q = ()

            if len(q):
                self.slug_iter = q.get().slug_iter + 1
                self.slug = '%s-%d' % (self.slug_base, self.slug_iter)
            else:
                self.slug = self.slug_base

        # Save the row
        super(Photo, self).save(*args, **kwargs) 

    class IKOptions:
        # This inner class is where we define the ImageKit options for the model
        spec_module = 'photos.specs'
        cache_dir = 'photos'
        image_field = 'original_image'
        save_count_as = 'num_views'

class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        exclude = ('user',)

class PhotoEditForm(ModelForm):
    class Meta:
        model = Photo
        exclude = ('original_image','user',)
