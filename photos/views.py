from django.views.generic.list_detail import object_list
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from mina_reboot.photos.models import Photo, UserProfile, UserProfileForm, PhotoForm, PhotoEditForm
 
def profile(request, username, template_name='profile.html'):
    photos = photos_by_user(username)

    context = {'photos':photos,
               'username':username}

    return render_to_response(template_name, context, context_instance=RequestContext(request))

@login_required
def manage_profile(request, template_name='manage_profile.html'):
    user_profile = UserProfile.objects.get(user__username__exact = request.user.username)

    # If the form has been submitted...
    if request.method == 'POST':
        form = UserProfileForm(request.POST, instance=user_profile) 
        if form.is_valid(): 
            form.save()
            request.user.message_set.create(message="Your profile was updated successfully.")
    else:
        form = UserProfileForm(instance=user_profile)

    return render_to_response('manage_profile.html', 
        {'form': form},
        context_instance=RequestContext(request)
    )

@login_required
def manage_photo(request, slug, template_name='manage_photo.html'):
    photo = Photo.objects.filter(user__username__exact = request.user.username).filter(slug=slug).get()

    # If the form has been submitted...
    if request.method == 'POST':
        form = PhotoEditForm(request.POST, instance=photo) 
        if form.is_valid(): 
            form.save()
            request.user.message_set.create(message="Your photo was updated successfully.")
    else:
        form = PhotoEditForm(instance=photo)

    return render_to_response(template_name,
        {'form': form, 'photo':photo},
        context_instance=RequestContext(request)
    )

@login_required
def upload_photo(request, template_name='upload_photo.html'):
    photo = Photo()
    photo.user = request.user

    # If the form has been submitted...
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES, instance=photo) 
        if form.is_valid(): 
            form.save()
            request.user.message_set.create(message="Your photo was uploaded successfully.")
            return dashboard(request)
    else:
        form = PhotoForm()

    return render_to_response(template_name, 
        {'form': form},
        context_instance=RequestContext(request)
    )

def photostream(request, username, template_name='photostream.html'):
    photos = photos_by_user(username)

    context = {'photos':photos,
               'username':username}

    return render_to_response(template_name, context, context_instance=RequestContext(request))

def photos_by_user(username):
    # Look up the user and 404 if not found
    user = get_object_or_404(User, username=username)

    # Get the user's latest photos
    return Photo.objects.filter(user__username__exact=username)

def photo_by_user_and_slug(request, username, slug, template_name='photo.html'):
    # Look up the user and 404 if not found
    user = get_object_or_404(User, username=username)

    # Get the user's latest photos
    photo = get_object_or_404(Photo, user__username__exact=username, slug__exact=slug)
    return render_to_response(template_name, {'photo': photo}, context_instance=RequestContext(request))

@login_required
def dashboard(request):
    try:
        user_profile = UserProfile.objects.all().filter(user__username__exact = request.user.username)
    except:
        new_profile = UserProfile()
        new_profile.user = request.user
        new_profile.save()
        request.user.message_set.create(message="Thanks for joining!  Please take a moment to create a profile.")
        return manage_profile(request)

    photos = Photo.objects.all()[:5]
    return render_to_response('dashboard.html', {'photos': photos}, context_instance=RequestContext(request))

'''
def openid_integration(request):
    if  request.openid :
        mlogger.debug("Found openid now checking for simple registration fields")

        email = None
        nickname = None
        password = None

        #fetch if openid provider provides any simple registration fields
     
        if request.openid.sreg :
            if request.openid.sreg.has_key('email') :
                email = request.openid.sreg['email']
                mlogger.debug("simple registration email"+email)
                
        if request.openid.sreg.has_key('nickname') :    
            nickname = request.openid.sreg['nickname']
            mlogger.debug("Your nickname is: %s"+nickname)            
                
        #check for already existing associations
        userassociation =  Openidassociation.objects.filter(openid=escape(str(request.openid)))
        openid = escape(str(request.openid))
        
        if userassociation :
            user = userassociation[0].user
            nickname = user.username
            email = user.email          
        else :
            iusername = False
         
            #no associations creating new user
            #generate some dummy password            
            if email is None :
                #for time being dummy email for openid user
                email =  UserManager().make_random_password()+"@"+UserManager().make_random_password()+".com"
                if nickname is None :
                #for time being dummy user name for openid user
                    iusername = True
                    nickname =  UserManager().make_random_password()+str(int(random.random()*100000000))               
               
                #create a user    
                user = CustomUser.objects.create_user(nickname,email)
            
                #until he get proper password
                user.invalidpassword = True
                user.first_log_openid = True
            
                #until he get proper username
                if iusername : 
                    user.invalidusername = True
                
                user.save()
                #create openid association
                os = Openidassociation()
                os.openid = escape(str(request.openid))
                os.user = user
                os.save()
        
            #authenticate and login
 
            customuser = authenticate(openid=openid)
            #please note we have authenticated in Registration form cleanup method
            login(request,customuser)          
            #redirect after post
            #if this is valid openid_next url the  redirect to it        
       
            if 'openid_next' in request.session :
                openid_next = request.session.get('openid_next')
                if len(openid_next.strip()) >  0 :
                    return HttpResponseRedirect(openid_next)    
            return HttpResponseRedirect("/oauth/")
'''
